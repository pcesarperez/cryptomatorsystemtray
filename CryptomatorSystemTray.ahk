; CryptomatorSystemTray.ahk
;
; Cryptomator is not able to start in background mode. This script launches
; Cryptomator and minimizes it to the System Tray.
;
; See: https://github.com/cryptomator/cryptomator/issues/418#issuecomment-342375391
;
; The original author of the script is Jekyll, aka 0x4a. I merely copied and
; adapted it for my own purposes.
;
; See https://github.com/0x4a


; General configuration.
#SingleInstance, Force
#NoEnv
#NoTrayIcon


; Useful variables.
CRYPTOMATOR_PATH := "C:\Program Files\Cryptomator\Cryptomator.exe"
MAX_RETRIES := 10
RETRY_WAIT := 3000 ; Milliseconds.


SplitPath, CRYPTOMATOR_PATH, CRYPTOMATOR_EXE


Process, Exist, %CRYPTOMATOR_EXE%
if (ErrorLevel > 0) {
    Exit
} else {
    counter := 0
    Loop, %MAX_RETRIES% {
        counter := counter + 1

        Run, %CRYPTOMATOR_PATH%, , , PID
        WinWait, ahk_pid %PID%
        WinClose, ahk_pid %PID%
        WinWaitClose, ahk_pid %PID%
        Sleep, %RETRY_WAIT%

        Process, Exist, %PID%
        if (ErrorLevel > 0) {
            Exit
        } else {
            Sleep, %RETRY_WAIT%
        }
    }
}